#include <iostream>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    
	int Mes;
	int Dia;
	Fecha()
    {

    }
	Fecha(uint mes, uint dia)
	{
		Mes = mes;
		Dia = dia;
	}
	
	uint mes()
	{
		return Mes;
	}

	uint dia()
	{
		return Dia;
	}
	
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif
	
	void incrementar_dia();

};

ostream& operator<<(ostream& os, Fecha f)
{
	os << f.Dia << "/" << f.Mes;
	return os;
}


#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
	bool igual_mes = this->mes() == o.mes();
    // Completar iguadad (ej 9)
    return igual_dia && igual_mes;
}
#endif

void Fecha::incrementar_dia() {
	uint mesActual = this->mes();
	uint cantidadDias = dias_en_mes(mesActual);
	if (this->dia() == cantidadDias)
	{
        this->Dia = 1;
		if (this->Mes == 12)
		{
            this->Mes = 1;
		}
		else
		{
            this->Mes = this->Mes + 1;
		}
	}
	else
	{
        this->Dia = this->Dia + 1;
	}
	
	
}

// Clase Horario

class Horario {
   public:
	uint Hora;
	uint Minuto;

	Horario()
    {

    }
	Horario(uint hora, uint minuto)
	{
		Hora = hora;
		Minuto = minuto;
	}
	
	uint hora()
	{
		return Hora;
	}

	uint min()
	{
		return Minuto;
	}
    
    bool operator==(Horario h);
	bool operator<(Horario h);
};

ostream& operator<<(ostream& os, Horario h)
{
	os << h.hora() << ":" << h.min();
	return os;
}

bool Horario::operator==(Horario h) {
    bool igual_hora = this->hora() == h.hora();
	bool igual_min = this->min() == h.min();
    return igual_hora && igual_min;
}

bool Horario::operator<(Horario h) {
    if (this->hora() > h.hora())
	{
		return false;
	}
	if (this->hora() < h.hora())
	{
		return true;
	}
	else
	{
		if (this->min() < h.min())
		{
			return true;
		}
	}
	return false;
}

// Clase Recordatorio

class Recordatorio{
   public:
	
	Fecha fecha;
	Horario horario;
	string mensaje;

	Recordatorio()
    {

    }
	Recordatorio (Fecha f, Horario h, string m)
	{
		fecha = f;
		horario = h;
		mensaje = m;
	}
};

ostream& operator<<(ostream& os, Recordatorio r)
{
	os << r.mensaje << " @ " << r.fecha.dia() << "/" << r.fecha.mes() << " " << r.horario.hora() << ":" << r.horario.min();
	return os;
}

// Clase Agenda
class Agenda {
   public:
	
	Fecha fecha = Fecha();
   
	Agenda(Fecha fecha_inicial)
	{
		fecha = fecha_inicial;
		recordatorios_de_hoy = {};
	}
	
	void agregar_recordatorio(Recordatorio rec);
	
	void incrementar_dia();

	vector<Recordatorio> TodosLosRecordatorios;
	vector<Recordatorio> recordatorios_de_hoy;

	void llenarRecsDeHoy();

	void ordenar_recordatorios();
	
	Fecha hoy()
	{
		return fecha;
	}
};

void Agenda::incrementar_dia(){
	this->fecha.incrementar_dia();
	this->recordatorios_de_hoy = {};
}



//ordeno con un especie de selection sort
void Agenda::ordenar_recordatorios() {
	int l = this->recordatorios_de_hoy.size();
	
	int i, j, mindex;
	for (i = 0; i < l-1; i++) 
	{
		mindex = i;
		for (j = i+1; j < l; j++)
		{
			if (this->recordatorios_de_hoy[j].horario < this->recordatorios_de_hoy[mindex].horario)
			{
				mindex = j;
			}
		}
		Recordatorio a = this->recordatorios_de_hoy[mindex];
		Recordatorio b = this->recordatorios_de_hoy[i];
		this->recordatorios_de_hoy[mindex] = b;
        this->recordatorios_de_hoy[i] = a;
	}
}

void Agenda::agregar_recordatorio(Recordatorio rec){
	this->TodosLosRecordatorios.push_back(rec);
}

void Agenda::llenarRecsDeHoy()
{
    int i = 0;
    int l = this->TodosLosRecordatorios.size();
    while (i < l)
    {
        if (this->TodosLosRecordatorios[i].fecha == this->hoy())
        {
            this->recordatorios_de_hoy.push_back(TodosLosRecordatorios[i]);
        }
        i++;
    }
}

string printRecs_(Agenda a)
{
    a.llenarRecsDeHoy();
    a.ordenar_recordatorios();
    string res = "";
    int i = 0;
    int l = a.recordatorios_de_hoy.size();
    while (i < l)
    {
        res = res + a.recordatorios_de_hoy[i].mensaje + " @ " +
              to_string(a.recordatorios_de_hoy[i].fecha.dia()) + "/" +
              to_string(a.recordatorios_de_hoy[i].fecha.mes())
              + " " + to_string(a.recordatorios_de_hoy[i].horario.hora()) + ":"
              + to_string(a.recordatorios_de_hoy[i].horario.min()) + "\n";
        i++;
    }
    return res;
}


ostream& operator<<(ostream& os, Agenda a)
{
	os << a.fecha << "\n" << "=====" << "\n" << printRecs_(a);
	return os;
}

